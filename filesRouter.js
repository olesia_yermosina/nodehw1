const express = require('express');
const router = express.Router();
const {createFile, getFiles, getFile, editFile, deleteFile} = require('./filesService.js')

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:name', getFile);

router.put('/:name', editFile);

router.delete('/:name', deleteFile)

module.exports = {router};