const fs = require('fs');
const path = require('path');

const regexp = new RegExp('.+\\.(?:log|txt|json|yaml|xml|js)$');

function createFile(req, res, next) {
    try{
        if(regexp.test(req.body.filename)){
            fs.writeFileSync(path.resolve('files', req.body.filename), req.body.content);
            res.status(200).send({'message': 'File created successfully'});
        }else{
            throw new Error('Incorrect file extension');
        }
    }catch (error) {
        next(error);
    }
}

function getFiles(req, res, next) {
    try{
        let files = fs.readdirSync('./files');
        console.log(files);
        res.status(200).send({'message': 'Success', 'files': files});
    }catch (error){
        next(error);
    }
}

function getFile(req, res, next) {
    try {
        let data = fs.readFileSync(path.resolve('files', req.params.name), 'utf-8');
        let filename = path.resolve('files', req.params.name);
        let {birthtime} = fs.statSync(filename);
        let ext = path.extname(filename).split('.')[1];

        res.status(200).send({
            'message': 'Success', 'filename': req.params.name, 'content': data,
            'extension': ext, 'uploadedDate': birthtime
        });
    } catch (error){
        next(error);
    }
}

function editFile(req, res, next) {
    try{
        if(regexp.test(req.body.filename)){
            fs.writeFileSync(path.resolve('files', req.params.name), req.body.content);
            res.status(200).send({'message': 'File edited successfully'});
        }else{
            throw new Error('Incorrect file extension');
        }
    }catch (error){
        next(error);
    }
}

function deleteFile(req, res, next) {
    try{
        fs.unlinkSync(path.resolve('files', req.params.name));
        res.status(200).send({'message': 'File deleted successfully'});
    }catch (error){
        next(error);
    }
}

module.exports = {createFile, getFiles, getFile, editFile, deleteFile};