const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const {router} = require('./filesRouter.js');

const PORT = 8080;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', router);
start();
app.use(errorHandler)

async function start() {
    try {
        if (!fs.existsSync('files')) {
            fs.mkdirSync('files');
        }
        app.listen(PORT);
    } catch (error) {
        console.log(`Error on server startup: ${error}`);
    }
}

function errorHandler(error, req, res, next) {
    let message;
    console.log(error);
    if(error.code === 'ENOENT'){
        message = 'File not found';
        res.status(400).send({'message': message});
    }else if(error.code === 'EISDIR'){
        message = 'Error on reading directory';
        res.status(500).send({'message': message});
    }
    res.status(400).send({'message': 'Error'});
}